<?php
namespace Erpk\Harserver\Controller;

use Erpk\Common\EntityManager;

abstract class Controller
{
    protected $request;
    protected $response;
    protected $entityManager = null;
    public $client;
    
    public function init()
    {
    }

    public function __construct($client, $params)
    {
        $this->client = $client;
        $this->params = $params;
    }

    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $this->entityManager = new EntityManager();
        }
        return $this->entityManager;
    }

    public function getParameter($key)
    {
        return isset($this->params[$key]) ? $this->params[$key] : null;
    }
}
