<?php
namespace Erpk\Harserver\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Erpk\Harserver\ViewModel;
use Erpk\Harvester\Module\JobMarket\JobMarketModule;

class JobMarketController extends Controller
{
    public function get()
    {
        $module = new JobMarketModule($this->client);
        $em = $this->getEntityManager();
        $countries = $em->getRepository('Erpk\Common\Entity\Country');
        
        $country = $countries->findOneByCode($this->getParameter('code'));
        $data = $module->scan($country, $this->getParameter('page'));
        $data['@nodeName'] = 'offer';
        $vm = new ViewModel($data);
        $vm->setRootNodeName('offers');
        return $vm;
    }
}
