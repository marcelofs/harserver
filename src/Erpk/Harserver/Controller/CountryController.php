<?php
namespace Erpk\Harserver\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Erpk\Harserver\ViewModel;
use Erpk\Harvester\Module\Country\CountryModule;
use Erpk\Common\Entity\Country;

class CountryController extends Controller
{
    protected function get($type)
    {
        $module = new CountryModule($this->client);
        $em = $this->getEntityManager();

        $countries = $em->getRepository('Erpk\Common\Entity\Country');
        $code = $this->getParameter('code');

        if (preg_match('/^[0-9]+$/', $code)) {
            $country = $countries->findOneById((int)$code);
        } else {
            $country = $countries->findOneByCode($code);
        }

        if ($country instanceof Country) {
            $data = $module->{'get'.$type}($country);

            switch ($type) {
                case 'Economy':
                    $data['embargoes']['@nodeName'] = 'embargo';
                    break;
                case 'Society':
                    $data['regions']['@nodeName'] = 'region';
                    break;
            }
            $vm = new ViewModel($data);
            $vm->setRootNodeName('country');
        } else {
            $vm = ViewModel::error('CountryNotFoundException', 500);
        }
        
        return $vm;
    }
    
    public function society()
    {
        return $this->get(
            'Society',
            $this->getParameter('code')
        );
    }
    
    public function economy()
    {
        return $this->get(
            'Economy',
            $this->getParameter('code')
        );
    }
}
