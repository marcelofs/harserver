<?php
namespace Erpk\Harserver\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Erpk\Harserver\ViewModel;
use Erpk\Harvester\Module\Market\MarketModule;
use Erpk\Common\EntityManager;

class MarketController extends Controller
{
    public function get()
    {
        $module = new MarketModule($this->client);
        $em = $this->getEntityManager();
        $countries = $em->getRepository('Erpk\Common\Entity\Country');
        $industries = $em->getRepository('Erpk\Common\Entity\Industry');
        
        $country  = $countries->findOneByCode($this->getParameter('country'));
        $industry = $industries->findOneByCode($this->getParameter('industry'));

        $data = $module->scan(
            $country,
            $industry,
            $this->getParameter('quality'),
            $this->getParameter('page')
        );
        
        $data['@nodeName']='offer';
        $vm = new ViewModel($data);
        $vm->setRootNodeName('offers');
        return $vm;
    }
}
