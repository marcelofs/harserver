<?php
(@include_once __DIR__ . '/../vendor/autoload.php') || @include_once __DIR__ . '/../../../autoload.php';

use Symfony\Component\Console\Application;
use Erpk\Harserver\Command\RunCommand;
use Erpk\Harserver\Command\ConfigCommand;

$app = new Application();
$app->add(new RunCommand());
$app->add(new ConfigCommand());
$app->run();
