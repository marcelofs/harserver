<?php
use Erpk\Harserver\Config;
use Erpk\Harserver\HttpKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

(@include_once __DIR__ . '/../vendor/autoload.php') || @include_once __DIR__ . '/../../../autoload.php';

if (file_exists(__DIR__.'/../config.json')) {
    $config = new Config(__DIR__.'/../config.json');
} else {
    $config = new Config(__DIR__.'/../../../config.json');
}

$request = Request::createFromGlobals();
$httpKernel = new HttpKernel($config);
$response = $httpKernel->handle($request);
$response->send();
